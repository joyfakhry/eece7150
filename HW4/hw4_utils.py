import numpy as np
import cv2
from matplotlib import pyplot as plt


def normalize(images):
    normalized_images = []
    clahe = cv2.createCLAHE()

    if type(images) == list:
        for image in images:
            normalized = clahe.apply(image)
            normalized_images.append(normalized)
    else:
        return clahe.apply(images)

    return normalized_images


def plot_features(img1, kp1, img2, kp2):
    fig, axs = plt.subplots(1, 2, figsize=(6, 5))

    img1 = cv2.drawKeypoints(img1, kp1, None, color=(0,255,0), flags=0)
    axs[0].imshow(img1)
    
    img2 = cv2.drawKeypoints(img2, kp2, None, color=(0,255,0), flags=0)
    axs[1].imshow(img2)

    axs[0].axis('off')
    axs[1].axis('off')
    fig.suptitle(f'Src and dst images, {len(kp1)} and {len(kp2)} features respectively')
    plt.show()


def plot_matches(matches, img1, kp1, img2, kp2):
    img = cv2.drawMatches(img1,kp1,img2,kp2,matches,None,flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
    # img = cv2.drawMatchesKnn(img1, kp1, img2, kp2, matches, None, flags=2)
            
    plt.figure(figsize=(10,6))
    plt.imshow(img)
    plt.title(f'Src and dst images, {len(matches)} matches')
    plt.axis('off')
    plt.show()


def plot_3d_points(points3d):
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    ax.scatter(xs=points3d[0], ys=points3d[1], zs=points3d[2])
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.set_title(f'{points3d.shape} points')


def make_projection(R,t,K):
    Rt = np.concatenate([R, t], axis=1)
    projection = K @ Rt

    return projection

def find_features(img):
    sift = cv2.SIFT_create(nfeatures=10000)
    key_points, descriptors = sift.detectAndCompute(img,None)

    return key_points, descriptors
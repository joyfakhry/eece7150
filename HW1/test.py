# Weather Simulator, prob 2.2
import numpy as np
import sympy
sympy.init_printing()

# 2b
state_transitions = {
    "sunny": {"sunny": 0.8, "cloudy": 0.2, "rainy": 0},
    "cloudy": {"sunny": 0.4, "cloudy": 0.4, "rainy": 0.2},
    "rainy": {"sunny": 0.2, "cloudy": 0.6, "rainy": 0.2}
}

start_day = "sunny"
num_days = 10


def weather_sequence(start_day, num_days, state_transitions):
    output_sequence = [start_day]
    for _ in range(num_days-1):
        next_day_distribution = state_transitions[output_sequence[-1]]
        next_day = np.random.choice(
            list(state_transitions.keys()), p=list(next_day_distribution.values()))

        output_sequence.append(next_day)

    return output_sequence


print(weather_sequence(start_day, num_days, state_transitions))
print()
# 2c
n = 10000


def static_dist_simulation(n, state_transitions):
    start_day = np.random.choice(list(state_transitions.keys()))
    run = weather_sequence(start_day, n, state_transitions)
    probs = np.array([run.count('sunny'), run.count(
        'cloudy'), run.count('rainy')]) / n

    return probs


# static_distribution = static_dist_simulation(n, state_transitions)
# for simulation in range(int(n/100)):
#     static_distribution += static_dist_simulation(n, state_transitions)

# print(static_distribution/sum(static_distribution))


# 2d
A = np.array([[0.8, 0.4, 0.2], [0.2, 0.4, 0.6], [0, 0.2, 0.2]])
print(A)

eigvals, eigvecs = np.linalg.eig(A)
D = np.diag(eigvals)
V = np.array(eigvecs)

S = V @ D @ np.linalg.inv(V)

print(D)
print('V:')
print(V)
print('inv V:')
print(np.linalg.inv(V))
print(np.around(S, decimals=5))

# print()
# print()

# M = sympy.Matrix([[0.8, 0.4, 0.2], [0.2, 0.4, 0.6], [0, 0.2, 0.2]])
# print(M)
# (P, D) = M.diagonalize()

# print()
# print(P)
# print()
# print(D)
# print()
# print(P * D * P.inv())

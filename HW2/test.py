import matplotlib.pyplot as plt
import cv2
import numpy as np
from PIL import Image, ImageOps


img1 = cv2.imread('HW2/HW2_image1.jpg')  # painting
img2 = cv2.imread('HW2/HW2_image2.jpg')  # image to place on painting

# src_pts and dst_pts correspond to the x_i and x_i' in homogeneous coordinates
# corner points of source image
src_pts = np.array([
    (0, 0, 1),
    (img2.shape[1], 0, 1),
    (img2.shape[1], img2.shape[0], 1),
    (0, img2.shape[0], 1),
])

# inner corners of picture frame, taken from plt.ginput
dst_pts = np.array([
    (532, 500, 1),
    (933, 461, 1),
    (960, 1147, 1),
    (565, 1092, 1)
])

# # Find the homography matrix
# H = cv2.findHomography(src_pts, dst_pts)

# # Apply the homography to the first image
# warped_img = cv2.warpAffine(img1, H, (img2.shape[1], img2.shape[0]))

# # Display the images
# cv2.imshow('Image 1', img1)
# cv2.imshow('Warped Image', warped_img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# x_1 = src_pts[0].T
# x_1_prime = dst_pts[0].T
# print(x_1)
# A_1 = np.zeros((2, 9))

# A_1[0][3:6] = -x_1.T
# A_1[0][6:9] = x_1_prime[1]*x_1.T
# A_1[1][0:3] = x_1.T
# A_1[1][6:9] = -x_1_prime[0]*x_1.T

# print(A_1)

# x_2 = src_pts[1].T
# x_2_prime = dst_pts[1].T

# A_2 = np.zeros((2, 9))

# A_2[0][3:6] = -x_2.T
# A_2[0][6:9] = x_2_prime[1]*x_2.T
# A_2[1][0:3] = x_2.T
# A_2[1][6:9] = -x_2_prime[0]*x_2.T
# print(A_2)

A = np.zeros((8, 9))
for row in range(4):
    x_i = src_pts[row].T
    x_i_prime = dst_pts[row].T

    # A_i = np.zeros((2, 9))

    A[row*2][3:6] = -x_i.T
    A[row*2][6:9] = x_i_prime[1]*x_i.T
    A[row*2+1][0:3] = x_i.T
    A[row*2+1][6:9] = -x_i_prime[0]*x_i.T

    # A = np.vstack([A, A_i])

for i in A:
    print(*i)
    print()

U, D, V = np.linalg.svd(A)

print(U)
print()
print(D)
print()
print(V)
print()

print(V[:, -1])

H = V.T[:, -1]

H = np.reshape(H, (3, 3))
print("H:")

H = H/H[2, 2]
print(H/H[2, 2])

x_1 = src_pts[0].T
x_1_prime = dst_pts[0].T
print()
print(np.cross(x_1, H*x_1_prime))


print('check H')

h, status = cv2.findHomography(src_pts, dst_pts)
print(h)
print(status)

print(dst_pts)
img2_warped = cv2.warpPerspective(img2, H, (img1.shape[1], img1.shape[0]))
cv2.imwrite('warped.png', img2_warped)
# plt.imshow(img2_warped)
# plt.show()

# cv2.imshow('background', img1)  # background
# cv2.waitKey(0)
# cv2.imshow('foreground', img2_warped)  # foreground
# cv2.waitKey(0)

# mask = cv2.imread('inverted_mask.png', cv2.IMREAD_GRAYSCALE)
mask = cv2.imread('mask.png')

print(mask.shape)
print(img2_warped.shape)


masked = cv2.bitwise_and(mask, img2_warped)
# cv2.imshow('masked', mask)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# result = cv2.addWeighted(img1, 1, img2_warped, 1, 0)
result = cv2.addWeighted(img1, 1, img2_warped, 0.5, 0, img1)

# cv2.imshow('Result', result)
# cv2.waitKey(0)
cv2.imwrite("Result.png", result)
# cv2.destroyAllWindows()


print(len(src_pts))

print(img2_warped[0][0])
